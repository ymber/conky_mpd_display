# Conky MPD track data

This script displays album art and track information for the track MPD is currently playing.

## Configuration

```lua
{
    album_art_position_x = 21,
    album_art_position_y = 21,
    album_art_size_x = 128,
    album_art_size_y = 128,

    time_bar_position_x = 160,
    time_bar_position_y = 144,
    time_bar_length = 300,
    time_bar_width = 10,
    time_bar_bg_rgba = {1, 1, 1, 0.2},
    time_bar_fg_rgba = {1, 1, 1, 0.5},

    text_position_x = 160,
    text_position_y = 113,
    text_line_gap = 4,
    text_font = "Droid Sans Mono",
    text_size = 20,
    text_rgba = {1, 1, 1, 0.8},
    text_lines = {
        "${mpd_elapsed}/${mpd_length}",
        "${mpd_album}",
        "${mpd_artist}",
        "${mpd_title}"
    },

    music_library = "/home/matthew/Music"
}
```

The `settings` table contains configuration options for the script.

* `album_art_position_x` is the x coordinate of the top left corner of the album art image.
* `album_art_position_y` is the y coordinate of the top left corner of the album art image.
* `album_art_size_x` is the x dimension of the album art image after resizing.
* `album_art_size_y` is the y dimension of the album art image after resizing.
* `time_bar_position_x` is the x coordinate of the left end of the elapsed time bar.
* `time_bar_position_y` is the y coordinate of the left end of the elapsed time bar.
* `time_bar_length` is the length in pixels of the elapsed time bar.
* `time_bar_width` is the width in pixels of the elapsed time bar.
* `time_bar_bg_rgba` is a table containing the rgba values for the elapsed time bar's background. Its elements should be red, green, blue, and alpha in that order.
* `time_bar_fg_rgba` is a table containing the rgba values for the elapsed time bar's foreground. Its elements should be red, green, blue, and alpha in that order.
* `text_position_x` is the x coordinate of the bottom left corner of the track data text.
* `text_position_y` is the y coordinate of the botoom left corner of the track data text.
* `text_line_gap` is the space in pixels between lines of track data text.
* `text_font` is the font used to render track data text.
* `text_size` is the height in pixels of the track data text.
* `text_rgba` is a table containing the rgba values for the track data text. Its elements should be red, green, blue, and alpha in that order.
* `text_lines` is a table containing the lines of text to print. Each element should be a single line of text. Each element is a string evaluated according to conky's text parsing rules every time the script executes. Text is printed from the bottom upwards - the first element of `text_lines` will be the bottom line when multiple elements are rendered.
* `music_library` is a path string to the root of your music library. It should be the same as MPD's music_directory config option.

## Calling from conky

```lua
lua_load = "/PATH/TO/SCRIPT",
lua_draw_hook_pre = "trackdata",
```

Inserting these lines in the conky.config table of your conky configuration file will call trackdata() every time the conky window refreshes.

## Requirements

This script requires conky to be compiled with lua support and lua bindings for cairo and imlib2.

Lua Dependencies:

* lua-http

* lualib_id3

* lua-cjson

* luafilesystem

Music Library Requirements:

* Only MP3 files are supported

* MP3 files must be tagged with Musicbrainz metadata
